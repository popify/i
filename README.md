### popify() plugin

**Description:** simple popup generator in pure JS  
**Author:** HT (@&#x200A;glenthemes)

---

#### Demo Preview:
:cherry_blossom:&ensp;[popify.gitlab.io/i/demo](https://popify.gitlab.io/i/demo)

#### Demo Code:
:hibiscus:&ensp;[jsfiddle.net/glenthemes/1k9p6574](https://jsfiddle.net/glenthemes/1k9p6574)

---

#### Requirements:
* you have **an existing button** (or similar) that you can bind the popup trigger to
* you have **existing popup HTML** present  
  (in other words, this plugin serves as the trigger, with a few more functionalities)
* intermediate knowledge of HTML and CSS  
  (it is assumed that your content's styling is more-or-less complete)

Example markup of your popup:  
> :bulb: Make sure to add the `hidden` attribute to your popup selector, like in the sample below.

```html
<div class="popup" hidden>
	<div class="popup-box">
		<p>Some text in your popup</p>
		
		<button class="close-popup">×</button>
	</div>
</div>
```

---

#### Step 1:
Initialize the script:
```html
<!--✻✻✻✻✻✻  POPIFY by @glenthemes  ✻✻✻✻✻✻-->
<script src="//popify.gitlab.io/i/popup.js"></script>
```

#### Step 2:
Run and configure (inside a `<script>`):
```javascript
popify({
	button: "button.box-trigger", // the selector to trigger the popup
	popup_selector: ".popup", // selector of the actual popup
	fade_speed: 400, // in milliseconds only
	exit_options: {
		exit_button: ".close-popup", // popup's close button
		click_outside_to_exit: true,
		escape_key_to_exit: true
	}
})
```

| Option Name | Explanation |
| ----------- | ----------- |
| `button` | an **existing** button (doesn't have to be a `button`) that you want to be served as the trigger for your popup to show |
| `popup_selector` | the selector for your **existing** popup |
| `fade_speed` | speed in `ms` (milliseconds) by which the popup fades in/out<br><br>※ *do not use seconds.* |
| `exit_button` | the selector for your **existing** popup's close button |
| `click_outside_to_exit` | `true` &#x2501; when clicked outside the box, close the popup<br><br>`false` &#x2501; when clicked outside the box, do nothing |
| `escape_key_to_exit` | `true` &#x2501; when `esc` key is pressed, close the popup<br><br>`false` &#x2501; when `esc` key is pressed, do nothing |

#### Step 3:
Add this CSS (inside a `<style>`):
```css
/*----- POPIFY STYLING -----*/
[popify]{
	position:fixed;
	top:0;left:0;
	width:100vw;
	height:100vh;
	backdrop-filter:blur(6px);
	z-index:10; /* increase this number if you can't see your popup */
	
	display:none;
	opacity:0;
}

[popify].show {
	display:flex;
	align-items:center;
	justify-content:center;
}
```

#### Usage notes:
* the parameters `button`, `popup_selector`, and `exit_button` need to be filled in order for the function to execute properly

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

---

#### Found this helpful?
Please consider leaving me a tip :revolving_hearts:  
&mdash;&ensp;[https://ko-fi.com/glenthemes](ko-fi.com/glenthemes)
