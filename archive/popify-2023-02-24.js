/*----------------------------------------------
    
    popify: simple popup generator in pure JS
    - written by HT (@glenthemes)
	
    gitlab.com/popify/i
    
----------------------------------------------*/

window.popify = function(boop){
	let button = boop.button;
	let pp_sel = boop.popup_selector;
	let fade_speed = boop.fade_speed;
	
	let exitButt = boop.exit_options.exit_button;
	let exitOut = boop.exit_options.click_outside_to_exit;
	let exitEsc = boop.exit_options.escape_key_to_exit;
	
	if(button && pp_sel && exitButt){
		document.addEventListener("DOMContentLoaded",() => {
			// add [popify] attr to popup
			// apply fade_speed transition to popup
			document.querySelectorAll(pp_sel).forEach(hxmlo => {
				hxmlo.setAttribute("popify","");
				hxmlo.style.transition = `opacity ${fade_speed}ms ease-in-out`;
			})
			
			/*---- BIND CLICK TO POPUP ----*/
			document.querySelectorAll(button).forEach(vwfhh => {
				vwfhh.addEventListener("click", (e) => {
					document.querySelectorAll(pp_sel).forEach(jpczj => {
						jpczj.classList.add("show");
						
						setTimeout(() => {
							jpczj.style.opacity = 1;
						},0)
					})
				})//end click
			})//end: button trigger forEach
			
			/*---- EXIT POPUP ----*/
			// standard button close
			let closePP = function(){
				document.querySelectorAll(pp_sel).forEach(pmfky => {
					pmfky.style.opacity = 0;
					setTimeout(() => {
						pmfky.classList.remove("show")
					},fade_speed)
				});
			}
			
			document.querySelectorAll(exitButt).forEach(bqhcr => {
				bqhcr.addEventListener("click", () => {
					closePP()
				})
			})
			
			// if: click_outside_to_exit == true
			if(exitOut){
				document.querySelectorAll(pp_sel).forEach(pmfky => {
					pmfky.addEventListener("click", (e) => {
						if(e.target == pmfky){
							closePP()						
						}
					})
				});
			}
			
			// if: press 'escape' to exit
			if(exitEsc){
				document.addEventListener("keydown", (e) => {
					if(e.keyCode == 27){
						closePP()
					}
				})
			}
		})//end: DOMContentLoaded
	}//end: check all params are filled
}//end function
