/*----------------------------------------------
    
    popify [v2]: popup generator in pure JS
    - written by HT (@glenthemes)
	
    gitlab.com/popify/i/v2
    
----------------------------------------------*/

window.popify = function(params){
    let popifyInit = (params) => {
        let button = params?.button;
        
        let popup = params?.popup_selector;
        let fade_speed = params?.fade_speed;		
		isNaN(fade_speed) ? fade_speed = 400 : "";
		
		let onMount = params?.actions?.onMount;
		let onMountButton = params?.actions?.onMountButton;
		let onMountBox = params?.actions?.onMountBox;
		
		let onFadedIn = params?.actions?.onFadedIn;
		let onFadedInButton = params?.actions?.onFadedInButton;
		let onFadedInBox = params?.actions?.onFadedInBox;
        
        let exitButton = params?.exit_options?.exit_button;
        let exitOut = params?.exit_options?.click_outside_to_exit;
        let exitEsc = params?.exit_options?.escape_key_to_exit;
		
		let onRemove = params?.actions?.onRemove;
		let onRemoveButton = params?.actions?.onRemoveButton;
		let onRemoveBox = params?.actions?.onRemoveBox;
		
		let onFadedOut = params?.actions?.onFadedOut;
		let onFadedOutButton = params?.actions?.onFadedOutButton;
		let onFadedOutBox = params?.actions?.onFadedOutBox;
		
		let getType = (x) => {
			return ({}).toString.call(x).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
		}
		
		/*=================================*/
		
		let selectorList = name => {
			let arr = [];
			let elOrList = el => el instanceof HTMLElement || (getType(el) === "nodelist" && el.length);
			
			let toPush = item => {
				let elements = (getType(item) === "string") ? document.querySelectorAll(item) : item;
				if(elOrList(elements)){
					elements instanceof NodeList || Array.isArray(elements) ?
					arr.push(...elements) : arr.push(elements)
				}
			}
			
			if(getType(name) === "string" || Array.isArray(name) || elOrList(name)){
				Array.isArray(name) ? name.forEach(toPush) : toPush(name);
			}
			
			return arr
		}
		
		/*=================================*/
		
		if(selectorList(button).length && selectorList(popup).length && (selectorList(exitButton).length || exitOut || exitEsc)){
			selectorList(popup)?.forEach(popup => {
				// add [popify] attr to popup
				// apply fade_speed transition to popup
				popup.setAttribute("popify","");
				popup.style.transition = `opacity ${fade_speed}ms ease-in-out`;
			})
			
			/*---- BIND CLICK TO POPUP ----*/
			selectorList(button)?.forEach(button => {
				button.addEventListener("click", (e) => {
					typeof onMount == "function" && onMount();
					typeof onMountButton == "function" && onMountButton(e.target);
					selectorList(popup)?.forEach(popup => {
						if(!popup.matches(".show")){
							popup.classList.add("show");
							popup.classList.add("fading-in");
							
							typeof onMountBox == "function" && onMountBox(popup);
							setTimeout(() => popup.style.opacity = 1, 0);
							setTimeout(() => {
								typeof onFadedIn == "function" && onFadedIn(popup);
								typeof onFadedInButton == "function" && onFadedInButton(e.target);
								typeof onFadedInBox == "function" && onFadedInBox(popup);
								
								popup.classList.remove("fading-in");
								popup.classList.add("faded-in");
							},fade_speed)
						}
					})
				})
			})
			
			/*---- EXIT POPUP ----*/
			let closePopup = () => {
				selectorList(popup)?.forEach(popup => {
					if(popup.matches(".show.faded-in")){
						typeof onRemove == "function" && onRemove();
						
						if(typeof onRemoveButton == "function"){
							selectorList(button)?.forEach(btn => {
								onRemoveButton(btn)
							})
						}
						
						typeof onRemoveBox == "function" && onRemoveBox(popup);
						
						popup.classList.remove("faded-in");
						
						popup.style.opacity = 0;
						popup.classList.add("fading-out");
						
						setTimeout(() => {
							typeof onFadedOut == "function" && onFadedOut();
							
							if(typeof onFadedOutButton == "function"){
								selectorList(button)?.forEach(btn => {
									onFadedOutButton(btn)
								})
							}
							
							typeof onFadedOutBox == "function" && onFadedOutBox(popup)
							
							popup.classList.remove("fading-out");
							popup.classList.remove("show");
						},fade_speed)
					}
				})
			}
			
			selectorList(exitButton)?.forEach(closeBtn => {
				closeBtn.addEventListener("click", () => {
					closePopup()
				})
			})
			
			// if: click_outside_to_exit == true
			if(exitOut){
				selectorList(popup).forEach(popup => {
                    popup.addEventListener("click", (e) => {
                        if(e.target == popup){
                            closePopup()
                        }
                    })
                });
			}
			
			// if: press "esc" to exit
			if(exitEsc){
				document.addEventListener("keydown", (e) => {
					if(e.keyCode == 27 || e.code === "Escape"){
						closePopup()
					}
				})
			}
			
		}//end: if required things exist
		
		/*=================================*/
    }//end popifyInit

    document.readyState == "loading" ?
    document.addEventListener("DOMContentLoaded", () => popifyInit(params)) :
    popifyInit(params);
}//end function